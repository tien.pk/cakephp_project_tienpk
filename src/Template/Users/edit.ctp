<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading">Actions</li>
        <li>
            <?= $this->Form->postLink(
                    __('Delete'),
                    ['action' => 'delete', $user->id],
                    ['confirm' => __('Are you sure you want to delete # {0}?', $user->id)]
                )
            ?>
        </li>
        <li><a href="/users">List Users</a></li>
    </ul>
</nav>
<div class="users form large-9 medium-8 columns content">
    <form action="/users/edit/<?= $user->id ?>" method="post" accept-charset="utf-8" enctype="multipart/form-data">
        <div style="display:none;">
            <input type="hidden" name="_method" value="PUT" />
            <input type="hidden" name="_csrfToken" autocomplete="off" value="<?= $this->request->getAttributes()['params']['_csrfToken']; ?>" />
        </div>
        <div class="form-group">
            <label for="fullname">Fullname</label>
            <input type="text" name="fullname" maxlength="100" id="fullname" value="<?= $user->fullname ?>"/>
        </div>
        <div class="form-group required">
            <label for="email">Email</label>
            <input type="email" name="email" required="required" maxlength="255" id="email" value="<?= $user->email ?>" readonly />
        </div>
        <div class="form-group">
            <label for="username">Username</label>
            <input type="text" name="username" maxlength="50" id="username" value="<?= $user->username ?>"/>
        </div>
        <div class="form-group">
            <label for="avatar">
                Avatar <br>
                <img src="<?= '/img/' . $user->avatar ?>" width="200px" alt="">
            </label>
            <input type="file" name="avatar" class="form-control" maxlength="1000" id="avatar" hidden/>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary col-2">Update</button>
        </div>
    </form>
</div>

<script type="text/javascript">
    function clickConfigm() {
        if(confirm('Are you sure delete it?')){
            alert('Deleted');
        }
    }
</script>
