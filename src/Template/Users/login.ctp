<?php
/**
 * @var \App\View\AppView $this
 */
?>
<div class="row d-flex justify-content-center mt-4">
    <div class="col-6">
        <h2>Login</h2>
        <form method="post" accept-charset="utf-8" action="/login" enctype="multipart/form-data">
            <div style="display:none;">
                <input type="hidden" name="_method" value="POST"/>
                <input type="hidden" name="_csrfToken" autocomplete="off" value="<?= $this->request->getAttributes()['params']['_csrfToken'] ?>"/>
            </div>
            <div class="form-group required">
                <label for="email">Email</label>
                <input type="email" name="email" required="required" maxlength="255" id="email"/>
            </div>
            <div class="form-group required">
                <label for="password">Password</label>
                <input type="password" name="password" required="required" id="password" minlength="6"/>
            </div>
            <div class="form-group d-flex justify-content-center">
                <button type="submit" class="btn btn-primary col-2">Login</button>
            </div>
            <div class="d-flex justify-content-end">
                <a href="/register" class="text-primary">Create one account?</a>
            </div>
        </form>
    </div>
</div>
