<?php
/**
 * @var \App\View\AppView $this
 */
?>
<div class="row d-flex justify-content-center mt-4">
    <div class="col-6">
        <h2>Register</h2>
        <form method="post" accept-charset="utf-8" action="/register" enctype="multipart/form-data">
            <div style="display:none;">
                <input type="hidden" name="_method" value="POST"/>
                <input type="hidden" name="_csrfToken" autocomplete="off" value="<?= $this->request->getAttributes()['params']['_csrfToken'] ?>"/>
            </div>
            <div class="form-group required">
                <label for="email">Email</label>
                <input type="email" class="form-control" name="email" required="required" maxlength="255" id="email"/>
            </div>
            <div class="form-group required">
                <label for="password">Password</label>
                <input type="password" class="form-control" name="password" required="required" id="password" minlength="6"/>
            </div>
            <div class="form-group required">
                <label for="password-cf">Password Confirm</label>
                <input type="password" class="form-control" name="password_cf" required="required" id="password" minlength="6"/>
            </div>
            <input type="file" class="form-control" name="avatar" hidden/>
            <div class="form-group mt-4 d-flex justify-content-center required">
                <button type="submit" class="btn btn-primary col-2">Register</button>
            </div>
            <div class="d-flex justify-content-end">
                <a href="/login" class="text-primary">You have one account</a>
            </div>
        </form>
    </div>
</div>
