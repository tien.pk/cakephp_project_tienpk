<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading">Actions</li>
        <li><a href="/users/edit/<?= $user->id ?>">Edit User</a> </li>
        <li><?= $this->Form->postLink(__('Delete User'), ['action' => 'delete', $user->id], ['confirm' => __('Are you sure you want to delete # {0}?', $user->id)]) ?> </li>
        <li><a href="/users">List Users</a> </li>
        <li><a href="/users/add">New User</a> </li>
    </ul>
</nav>
<div class="users view large-9 medium-8 columns content">
    <h3 class="header">View User</h3>
    <table class="vertical-table">
        <tr>
            <th scope="row">Fullname</th>
            <td><?= h($user->fullname) ?></td>
        </tr>
        <tr>
            <th scope="row">Email</th>
            <td><?= h($user->email) ?></td>
        </tr>
        <tr>
            <th scope="row">Username</th>
            <td><?= h($user->username) ?></td>
        </tr>
        <tr>
            <th scope="row">Avatar</th>
            <td><img src="<?= '/img/' . h($user->avatar) ?>" width="150px" alt=""></td>
        </tr>
        <tr>
            <th scope="row">Created</th>
            <td><?= h($user->created) ?></td>
        </tr>
        <tr>
            <th scope="row">Modified</th>
            <td><?= h($user->modified) ?></td>
        </tr>
    </table>
</div>
