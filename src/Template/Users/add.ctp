<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Users'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="users form large-9 medium-8 columns content">
    <h2>Add User</h2>
    <form method="post" accept-charset="utf-8" action="/users/add" enctype="multipart/form-data">
        <div style="display:none;">
            <input type="hidden" name="_method" value="POST"/>
            <input type="hidden" name="_csrfToken" autocomplete="off" value="<?= $this->request->getAttributes()['params']['_csrfToken'] ?>"/>
        </div>
        <div class="form-group required">
            <label for="fullname">Fullname</label>
            <input type="text" name="fullname" maxlength="100" id="fullname"/>
        </div>
        <div class="form-group required">
            <label for="email">Email</label>
            <input type="email" name="email" required="required" maxlength="255" id="email"/>
        </div>
        <div class="form-group required">
            <label for="username">Username</label>
            <input type="text" name="username" maxlength="50" id="username"/>
        </div>
        <div class="form-group required">
            <label for="avatar">Avatar</label>
            <input type="file" name="avatar" maxlength="1000" id="avatar" class="form-control"/>
        </div>
        <div class="form-group required">
            <label for="password">Password</label>
            <input type="password" name="password" required="required" id="password"/>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary col-2">Add</button>
        </div>
</form>

</div>
