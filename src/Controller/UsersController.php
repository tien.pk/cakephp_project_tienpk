<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $search = $this->request->getQuery('search');

        if ($search) {
            $query = $this->Users
                ->find('all')
                ->where(
                    [
                        'OR' => [
                            'username like' => '%' . $search . '%',
                            'email like' => '%' . $search . '%'
                        ]
                    ]
                );
        } else {
            $query = $this->Users;
        }

        $users = $this->paginate($query);
        $this->set(compact('users'));
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => [],
        ]);

        $this->set('user', $user);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $user = $this->Users->newEntity();

        if ($this->request->is('post')) {

            $data = $this->request->getData();
            $avatar = $data['avatar'];

            if(!empty($avatar)){

                if (!$this->checkPathFile($avatar)){
                    $this->Flash->error("The path file is must 'png' or 'jpg' ");
                    return $this->redirect(['action' => 'add']);
                }

                elseif (!$this->checkSizeFile($avatar)){
                    $this->Flash->error("The size file max 2MB ");
                    return $this->redirect(['action' => 'add']);
                }

                else {
                    $fileName = date('YmdHis') . '-' . $avatar['name'];
                    $uploadPath = WWW_ROOT . 'img/';
                    $uploadFile = $uploadPath . $fileName;

                    if (move_uploaded_file($avatar['tmp_name'], $uploadFile)){
                        $data['avatar'] = $fileName;
                    }
                }
            }

            $user = $this->Users->patchEntity($user, $data);
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $this->set(compact('user'));
    }

    public function login(){
        if ($this->request->is('post')) {
            $data = $this->request->getData();

            if(strlen($data['password']) < 6){
                $this->Flash->error('Password min 6 characters');
                return $this->redirect('/login');
            }

            $user = $this->Auth->identify();

            if ($user) {
                $this->Auth->setUser($user);
                return $this->redirect('/users');
            }

            $this->Flash->error('Your email or password is incorrect.');
        }
    }

    public function register(){
        $user = $this->Users->newEntity();

        if ($this->request->is('post')) {
            $data = $this->request->getData();
            $data['avatar'] = 'avatar.png';

            if($this->checkPass($data['password'], $data['password_cf'])){

                if(strlen($data['password']) >= 6) {

                    unset($data['password_cf']);
                    $user = $this->Users->patchEntity($user, $data);

                    if ($this->Users->save($user)) {
                        $this->Flash->success(__('The user had registered.'));
                        return $this->redirect('/login');
                    }

                    else {
                        $this->Flash->error(__('The user could not be saved. Please, try again.'));
                        return $this->redirect('/register');
                    }

                }

                else {
                    $this->Flash->error(__('The password min 6 characters. Please, try again.'));
                    return $this->redirect('/register');
                }

            }

            $this->Flash->error(__('The password confirm wrong. Please, try again.'));
            return $this->referer();

        }

        $this->set(compact('user'));
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => [],
        ]);

        if ($this->request->is(['patch', 'post', 'put'])) {

            $data = $this->request->getData();
            $avatar = $data['avatar'];
            $oldAvatar = $user->avatar;

            if($oldAvatar != $avatar['name']) {

                if(!empty($avatar['name'])) {

                    if (!$this->checkPathFile($avatar)) {
                        $this->Flash->error("The path file is must 'png' or 'jpg' ");
                        return $this->redirect(['action' => 'edit', $user->id]);
                    }

                    elseif (!$this->checkSizeFile($avatar)) {
                        $this->Flash->error("The size file max 2MB ");
                        return $this->redirect(['action' => 'edit', $user->id]);
                    }

                    else {

                        $fileName = date('YmdHis') . '-' . $avatar['name'];
                        $uploadPath = WWW_ROOT . 'img/';
                        $uploadFile = $uploadPath . $fileName;

                        if (move_uploaded_file($avatar['tmp_name'], $uploadFile)) {
                            $data['avatar'] = $fileName;
                        }
                    }
                }

                else {
                    $data['avatar'] = $oldAvatar;
                }
            }

            $user = $this->Users->patchEntity($user, $data);

            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been updated.'));

                return $this->redirect(['action' => 'edit', $user->id]);
            }

            $this->Flash->error(__('The user could not be update. Please, try again.'));
        }

        $this->set(compact('user'));
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    /**
     * @return \Cake\Http\Response|null
     */
    public function logout(){
        $this->Flash->success('You are now logged out.');
        return $this->redirect($this->Auth->logout());
    }

    /**
     * @param $file
     * @return bool
     */
    public function checkPathFile($file){
        $pathAllow = ['png', 'jpg'];
        $pathFile = pathinfo($file['name'], PATHINFO_EXTENSION);

        if(in_array($pathFile, $pathAllow)) {
            return true;
        }

        return false;
    }

    /**
     * @param $file
     * @return bool
     */
    public function checkSizeFile($file){
        $maxSize = 1024 * 1024 * 2;
        $fileSize = $file['size'];

        if($fileSize < $maxSize) {
            return true;
        }

        return false;
    }

    /**
     * @param $pass1
     * @param $pass2
     * @return bool
     */
    public function checkPass($pass1, $pass2) {
        if($pass2 === $pass1) {
            return true;
        }

        return false;
    }

//    public function checkEmail($email){
//        if($this->Users->find)
//    }
}
